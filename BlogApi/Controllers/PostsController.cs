﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;

namespace BlogApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly baseContext _context;

        public PostsController(baseContext context)
        {
            _context = context;
        }

        // GET: Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Post>>> GetPosts()
        {
            // no pido todos los datos de la base porque no hacen falta, y encima si hay imagenes muy pesadas
            // (no valide peso de archivos), el select * from post puede tardar mucho, degradando la performance
            // TODO: ver si hay alguna mejor forma de pedir esos datos sin crear todo el objeto Post
            // (como objetos anonimos, o crear otro viewmodel)...
            var posts = (from post in _context.Posts
                         orderby post.FechaCreacion descending
                         select new Post { 
                         Id = post.Id,
                         Titulo = post.Titulo
                         }).ToListAsync();
            return await posts;
        }

        // GET: Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            /* Si despues hago que la imagen vaya por otro lado, de esta forma excluyo la
               imagen en la consulta
            var post = await (from _post in _context.Posts
                              where _post.Id == id
                              select new Post
                              {
                                  Id = _post.Id,
                                  Titulo = _post.Titulo,
                                  Contenido = _post.Contenido,
                                  Categoria = _post.Categoria,
                                  FechaCreacion = _post.FechaCreacion
                              }).FirstOrDefaultAsync();*/

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }

        // PUT: Posts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, Post post)
        {
            if (id != post.Id)
            {
                return BadRequest();
            }

            _context.Entry(post).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: Posts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(Post post)
        {
            // agrego timestamp
            post.FechaCreacion = DateTime.Now;
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPost", new { id = post.Id }, post);
        }

        // DELETE: Posts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Post>> DeletePost(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            return post;
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
