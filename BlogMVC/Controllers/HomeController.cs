﻿using BlogMVC.Helper;
using BlogMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlogMVC.Controllers
{
    public class HomeController : Controller
    {
        PostAPI _api = new PostAPI();

        public async Task<IActionResult> Index()
        {
            List<PostViewModel> posts = new List<PostViewModel>();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync("Posts");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;

                // Las options son para que en el deserializado se maneje camelCase (no funciona sin estas opciones)
                var options = new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                posts = JsonSerializer.Deserialize<List<PostViewModel>>(results, options);
            }
            return View(posts);
        }

        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        public IActionResult New(PostViewModel post)
        {
            if (ModelState.IsValid)
            {
                var img = post.ImgFile;

                // hay que pasar de la imagen a byte[]
                if (img != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        post.ImgFile.CopyTo(ms);
                        post.Imagen = ms.ToArray();
                    }
                }


                HttpClient client = _api.Initial();

                // mando el post a la api
                var postTask = client.PostAsJsonAsync<PostViewModel>("Posts", post);
                postTask.Wait();

                // ya se realizo la creacion del post en la api
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            return View(post);
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var post = new PostViewModel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.DeleteAsync($"Posts/{Id}");

            if(!res.IsSuccessStatusCode)
            {
                TempData["errorCode"] = res.StatusCode;
                return RedirectToAction("Error");
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(int Id)
        {
            var post = new PostViewModel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"Posts/{Id}");
            if(res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                var options = new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                post = JsonSerializer.Deserialize<PostViewModel>(results, options);
            }
            else
            {
                TempData["errorCode"] = res.StatusCode;
                return RedirectToAction("Error");
            }

            return View(post);
        }
        public async Task<FileResult> GetImage(int Id)
        {
            // buscar en la api la imagen
            
            var post = new PostViewModel();

            // TODO: hacer un metodo en la api q devuelva solo la imagen, y hacer que en el get del post por id no devuelva la imagen
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"Posts/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                var options = new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                post = JsonSerializer.Deserialize<PostViewModel>(results, options);
            }
            
            return File(post.Imagen, "image/png"); // estoy limitando solo el uso de .png
        }

        public async Task<IActionResult> Edit(int Id)
        {
            var post = new PostViewModel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"Posts/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                var options = new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                post = JsonSerializer.Deserialize<PostViewModel>(results, options);
            }
            else
            {
                TempData["errorCode"] = res.StatusCode;
                return RedirectToAction("Error");
            }

            // para arreglar el tema de los espacios en titulo y categoria
            post.Titulo = post.Titulo.Trim();
            post.Categoria = post.Categoria.Trim();

            return View(post);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PostViewModel post)
        {
            if (post.ImgFile != null)
            { // si se adjunto una imagen, pasarla a byte[] para subir a la base
                var img = post.ImgFile;

                // hay que pasar de la imagen a byte[]
                if (img != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        post.ImgFile.CopyTo(ms);
                        post.Imagen = ms.ToArray();
                    }
                }
            }
            // si no se adjunto una imagen, manda la q ya estaba
            // el tema es que se vuelve a guardar en la base de datos la misma imagen
            // o sea, se sobreescribe la imagen consigo misma

            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.PutAsJsonAsync($"Posts/{post.Id}", post);

            if (res.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View(post);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
