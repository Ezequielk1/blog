﻿using BlogMVC.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogMVC.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is obligatory")]
        [StringLength(10)]
        public string Titulo { get; set; }

        [Display(Name = "Message")]
        [Required(ErrorMessage = "Message is needed")]
        public string Contenido { get; set; }

        [Display(Name = "Image")]
        public byte[] Imagen { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please select a category")]
        [StringLength(10)]
        public string Categoria { get; set; }

        [Display(Name = "Date")]
        public DateTime FechaCreacion { get; set; }

        [Required(ErrorMessage = "Please select an image (.png only)")]
        [DataType(DataType.Upload)]
        [AllowedExtensions(new string[] { ".png" })]
        public IFormFile ImgFile { set; get; }
    }
}
