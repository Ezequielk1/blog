﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlogMVC.Helper
{
    public class PostAPI
    {
        public HttpClient Initial()
        {
            // para bypassear el certificado (me tiraba error de certificados)
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            var client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri("https://localhost:44301/");
            return client;
        }
    }
}
